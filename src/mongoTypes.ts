import { prop } from '@typegoose/typegoose'

export enum BackupSetting {
    LOCAL,
    NEW_PLAYLIST,
    APPEND_PLAYLIST,
    ADD_TO_LIBRARY,
    NO_BACKUP
}

export enum BackupType {
    FORCED,
    AUTOMATIC
}

export class ArtistItem {

    @prop({ required: true })
    public artistName!: string;

    @prop({ required: true })
    public artistURI!: string;
}

export class TrackItem {
    @prop({ required: true })
    public trackName!: string;

    @prop({ required: true })
    public explicit!: boolean;

    @prop({ required: true })
    public trackURI!: string;
}

export class AlbumItem {
    @prop({ required: true })
    public albumName!: string;

    @prop({ required: true })
    public albumURI!: string;
}

export class PlaylistTrackItem {
    @prop({ required: true, kind: Array, type: ArtistItem })
    public artist!: ArtistItem[];

    @prop({ required: true })
    public track!: TrackItem;

    @prop({ required: true })
    public album!: AlbumItem;

    @prop({ required: true })
    public duration!: string;
}

export class PlaylistToBackup {
    @prop({ required: true })
    public playlistId!: string;

    @prop({ required: true })
    public playlistName!: string;

    @prop({ required: true })
    public playlistDesc!: string;

    @prop({ required: true })
    public playlistURI!: string;
}

export class PlaylistInfo extends PlaylistToBackup {
    @prop({})
    public playlistDate?: number;

    @prop({ required: true })
    public playlistImage!: string;

    @prop({ required: true, kind: Array, type: PlaylistTrackItem })
    public playlistTracks!: PlaylistTrackItem[];
}

export class UserClass {
    @prop({ required: true })
    public userID!: string;

    @prop({ required: true })
    public username!: string;

    @prop({ required: true })
    public accessToken!: string;

    @prop({ required: true })
    public refreshToken?: string;

    @prop({ required: true, set: (val: number) => Math.floor(Date.now() / 1000) + val, get: (val: number) => val })
    public expires_in!: number;

    @prop({ required: true, kind: Array, type: PlaylistToBackup })
    public playlists!: PlaylistToBackup[];

    @prop({ kind: Array, type: PlaylistInfo })
    public backups?: PlaylistInfo[];

    @prop({ enum: BackupSetting })
    public backupSetting?: BackupSetting;

    @prop()
    public backupPlaylistId?: string;

    @prop()
    public backupTime?: number;
}

export class BackupLogs {
    @prop({ required: true })
    public userID!: string;

    @prop({ required: true })
    public backupTime!: number;

    @prop({ required: true })
    public success!: boolean;

    @prop({ enum: BackupSetting })
    public backupSetting?: BackupSetting;

    @prop({require: true})
    public backupMessage?: string

    @prop({ required: true, enum: BackupType })
    public backupType!: BackupType
}
