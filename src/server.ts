import express, { json, text } from "express";
import mongoose from 'mongoose';
import session from 'express-session';
import passport, { AuthenticateOptions } from 'passport';
import cors from 'cors';
import axios from 'axios'
import queryString from 'querystring'
import { Strategy } from 'passport-spotify';
import Spotify from 'spotify-web-api-node'
import textToImage from 'text-to-image'
import moment = require('moment');
import * as nodeSchedule from 'node-schedule'
import Bottleneck from "bottleneck";
import {UserClass, PlaylistInfo, PlaylistToBackup, PlaylistTrackItem, BackupSetting, BackupLogs, BackupType, ArtistItem, TrackItem} from './mongoTypes'
import connectMongo from 'connect-mongo';
const MongoStore = connectMongo(session);

import {getModelForClass} from '@typegoose/typegoose';

/**
 * -------------- GENERAL SETUP ----------------
 */

import { config } from "dotenv"
config();

textToImage.registerFont("./fonts/Montserrat-Bold.ttf", { family: "Montserrat" });

const app = express();

const isDevelopment = process.env.NODE_ENV?.trim() === 'development'
let callbackURL = 'https://rediscovr.co.uk/spotify/callback'
if (isDevelopment) {
    callbackURL = "http://localhost:3000/spotify/callback"
}
console.log(callbackURL)

let whitelist: string[] = []
if (isDevelopment)
{
    whitelist = ['http://localhost:8080', 'http://localhost:3000', 'localhost:8080', 'localhost:3000']
}
else 
{
    whitelist = ['https://rediscovr.co.uk', 'https://www.rediscovr.co.uk']
}
let corsOptions = {
    origin: function (origin: any, callback: any) {
        if (origin === undefined || whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error(origin + 'Not allowed by CORS'))
        }
    },
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    credentials: true,
    optionsSuccessStatus: 204
}

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/**
 * -------------- BOTTLENECK ----------------
 */
const bottleneck = new Bottleneck({maxConcurrent: 5, minTime:15000})

/**
 * -------------- DATABASE ----------------
 */
const conn = process.env.DB_STRING;

const connection = mongoose.createConnection(conn!, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

connection.on('connecting', () => {
    console.log('connected');
});

connection.catch(e => {
    console.log(e);
})

const User = getModelForClass(UserClass, { existingConnection: connection });
const BackupLog = getModelForClass(BackupLogs, { existingConnection: connection });

const SpotifyStrat = new Strategy(
    {
        clientID: process.env.clientID!,
        clientSecret: process.env.clientSecret!,
        callbackURL: callbackURL
    },
    function (accessToken, refreshToken, expires_in, profile, done) {
        process.nextTick(function () {
            User.findOne({ userID: profile.id })
                .then((user) => {
                    if (!user) {
                        User.create({
                            userID: profile.id,
                            username: profile.username,
                            accessToken: accessToken,
                            refreshToken: refreshToken,
                            expires_in: expires_in!,
                            playlists: [],
                        })
                            .then(res => {
                                console.log("Memes" + res);
                                done(null, res);
                            })
                            .catch(e => {
                                console.log(e);
                            })
                    }
                    else {
                        return done(null, user);
                    }
                })
                .catch((err) => {
                    done(err);
                });
        })

    })

passport.use(SpotifyStrat);

/**
 * -------------- SERIALISATION ----------------
 */

passport.serializeUser(function (user: mongoose.Document, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
    User.findById(id, function (err, user) {
        if (err) { return cb(err); }
        cb(null, user);
    });
});

/**
 * -------------- SESSION SETUP ----------------
 */
const sessionStore = new MongoStore({ mongooseConnection: connection, collection: 'sessions' })
app.use(session({
    secret: process.env.SECRET!,
    resave: false,
    saveUninitialized: true,
    store: sessionStore,
    unset: "destroy",
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 // 1 Day
    }
}));

/**
 * -------------- PASSPORT AUTHENTICATION ----------------
 */
app.use(passport.initialize());
app.use(passport.session());

/**
 * -------------- ROUTES ----------------
 */

interface SpotifyAuthOptions extends AuthenticateOptions
{
    showDialog: boolean
}

app.get(
    '/spotify/auth', ensureUnauthenticated,
    passport.authenticate('spotify', {
        scope: ['user-read-email', 'user-read-private', 'playlist-read-private', 'playlist-modify-private', 'user-library-modify', 'user-library-read',
        'playlist-modify-public', 'playlist-read-collaborative'],
        showDialog: true
    } as SpotifyAuthOptions,
        () => { }
    ));

app.get(
    '/spotify/callback',
    passport.authenticate('spotify', { failureRedirect: '/login' }),
    function (req, res) {
        if(isDevelopment)
        {
            res.redirect('http://localhost:8080/profile/playlists')
        }
        else
        {
            res.redirect('/profile/playlists')
        }
    }
);

app.get(
    '/getUser', ensureAuthenticated, (req, res) => {
        res.json(req.user);
    }
);

app.post('/saveSettings', ensureAuthenticated, (req, res) => {
    const user = new User(req.user);
    console.log("Saving settings for: " + user.userID);
    if (req.body.playlists.length && req.body.backupSetting !== null) {
        User.findById(user._id, (err, result) => {
            if (result) {
                result.playlists = req.body.playlists;
                result.backupSetting = req.body.backupSetting;
                if (req.body.backupPlaylistId) {
                    result.backupPlaylistId = req.body.backupPlaylistId;
                }
                result.save()
                    .then(result2 => {
                        res.json({ success: true })
                    }).catch(e => {
                        console.log(e);
                        res.json({ success: false, error: e });
                    });
            }
            else {
                console.log(err)
            }
        })
    }
});

app.post('/getUserPlaylists', ensureAuthenticated, ensureSpotifyTokenValid, (req, res) => {
    const user = new User(req.user);
    User.findById(user._id, (err, dbResult) => {
        if (dbResult) {
            const spotify = new Spotify();
            spotify.setAccessToken(dbResult.accessToken)

            let playlists: PlaylistToBackup[] = [];
            spotify.getUserPlaylists(dbResult.userID, { limit: 50 })
                .then(result => {
                    result.body.items.forEach((playlist) => {
                        if (playlist.owner.id == (req.body.playlistOwner || user.userID)) {
                            const tempPlaylist = playlist as SpotifyApi.PlaylistObjectFull;
                            playlists.push({
                                playlistId: tempPlaylist.id,
                                playlistName: tempPlaylist.name,
                                playlistDesc: (tempPlaylist.description || ""),
                                playlistURI: tempPlaylist.uri
                            });
                        }
                    });
                    res.json({ playlists: playlists })
                })
                .catch(e => {
                    console.log(e)
                    throw new Error(e);
                })
        }
    })
})

app.get('/forceBackup', ensureAuthenticated, ensureSpotifyTokenValid, (req, res) => {
    backupPlaylists(req.user)
        .then(response => {
            res.json(response);
        })
        .catch(e => {
            res.json(e)
        })
});

app.get('/getLogs', ensureAuthenticated, (req, res) => {
    const user = new User(req.user);
    BackupLog.find({userID: user.userID}, (err, dbResult) => {
        console.log()
        res.json({backupLogs: dbResult})
    });
})

// Visiting this route logs the user out
app.get('/logout', ensureAuthenticated, (req, res, next) => {
    req.logout();
    req.session?.destroy(e=> {if(e) console.log(e)});
    res.json({success:true})
});

/**
 * -------------- SERVER ----------------
 */

// Server listens on http://localhost:3000
app.listen(3000);
console.log("Listening");

/**
 * -------------- HELPERS ----------------
*/

// TO-DO Shouldn't be doing another DB lookup in here when the Auto backup already has done a DB lookup...
// TO-DO Split into independent functions for readablilty? Look into.
function backupPlaylists(reqUser?: any, userDBID?: string): Promise<any> {
    return new Promise((resolve, reject) => {
        let userID: string;
        let forced = BackupType.FORCED;

        if (reqUser) {
            userID = new User(reqUser)._id;
        }
        else if (userDBID) {
            forced = BackupType.AUTOMATIC;
            userID = userDBID
        }

        console.log("Backup For " + userID!); 

        User.findById(userID!, (err, dbResult) => {
            if (dbResult) {
                const spotifySession = new Spotify({ accessToken: dbResult.accessToken, refreshToken: dbResult.refreshToken! });
                switch (dbResult.backupSetting) {
                    case BackupSetting.NO_BACKUP: {
                        break;
                    }
                    case BackupSetting.LOCAL: {
                        let promises: any = [];
                        for (const playlist in dbResult.playlists) {
                            if (!dbResult.backups) {
                                dbResult.backups = []
                                promises.push(findPlaylistSongs(dbResult.playlists[playlist], spotifySession))
                            }
                            else if (dbResult.backups.length == 0) {
                                promises.push(findPlaylistSongs(dbResult.playlists[playlist], spotifySession))
                            }
                            else if (!dbResult.backups?.some(el => (el.playlistName == dbResult.playlists[playlist].playlistName)
                                && (el.playlistDate == moment().day(moment().day() >= 5 ? 5 : -2).startOf('day').valueOf()))) {
                                promises.push(findPlaylistSongs(dbResult.playlists[playlist], spotifySession))
                            }
                            else {
                                console.log("Dupe Found " + dbResult.playlists[playlist].playlistName);
                            }
                        }
                        Promise.all(promises)
                            .then(responses => {
                                responses.forEach(playlistRes => {
                                    if(!dbResult.backups?.some(el => el.playlistDate == (playlistRes as PlaylistInfo).playlistDate))
                                    {
                                        dbResult.backups?.push(playlistRes as PlaylistInfo)
                                    }
                                    else 
                                    {
                                        console.log("Dupe Found " + (playlistRes as PlaylistInfo).playlistName)
                                    }
                                })
                                if (dbResult.isModified()) {
                                    dbResult.save()
                                        .then(dbRes => {
                                            createLog(dbRes.userID, true, forced, BackupSetting.LOCAL)
                                            .then(res => {
                                                resolve({ backups: dbRes.backups! });
                                            })
                                        })
                                        .catch(e => {
                                            createLog(dbResult.userID, false, forced, BackupSetting.LOCAL, e.message)
                                            .then(res => {
                                                console.log(e);
                                                reject({ error: e.message })
                                            });
                                        });
                                }
                                else {
                                    createLog(dbResult.userID, false, forced, BackupSetting.LOCAL, "Current Weeks Playlists Already Backed Up, Please Check Your Playlists Page.")
                                    .then(res => {
                                        reject({ error: "Current Weeks Playlists Already Backed Up, Please Check Your Playlists Page." });
                                    });
                                }
                            }).catch(e => {
                                createLog(dbResult.userID, false, forced, BackupSetting.LOCAL, e.message)
                                .then(res => {
                                    console.log(e);
                                    reject({ error: e.message })
                                });
                            });
                        break;
                    }
                    case BackupSetting.NEW_PLAYLIST: {
                        try {
                            let userPlaylists: string[] = []
                            spotifySession.getUserPlaylists(dbResult.userID, { limit: 50 })
                            .then(result => {

                                let playlistPromises = [];
                                userPlaylists.push(...result.body.items.map(el => el.name));
                                if (result.body.total > 100) {
                                    for (let i = 1; i < Math.ceil(result.body.total / 100); i++) {
                                        playlistPromises.push(spotifySession.getUserPlaylists(dbResult.userID, { limit: 50, offset: 100 * i}))
                                    }
                                }

                                Promise.all(playlistPromises)
                                .then(playlistResponses => {
                                    playlistResponses.forEach(playlistResponse => {
                                        userPlaylists.push(...playlistResponse.body.items.map(el => el.name))
                                    })

                                    let weekNumber = moment().isoWeek();
                                    if (moment().day() < 5) {
                                        weekNumber -= 1;
                                    }

                                    let playlistsToBackup: {playlist: PlaylistToBackup, newPlaylistName: string}[] = []
                                    dbResult.playlists.forEach(playlist=>{
                                        playlistsToBackup.push({playlist: playlist, 
                                            newPlaylistName: playlist.playlistName + " (Week " + weekNumber + " Year " + moment().year() + ")" })
                                    })

                                    console.log(playlistsToBackup);
                                    const playlsitsToAdd = playlistsToBackup.filter(el => !userPlaylists.includes(el.newPlaylistName))

                                    if (playlsitsToAdd.length > 0)
                                    {
                                        let playlistToAddPromises: Promise<any>[] = []
                                        playlsitsToAdd.forEach(playlistToAdd => {
                                            let promiseToAdd = new Promise((resolve, reject) => {
                                                spotifySession.createPlaylist(dbResult.userID, playlistToAdd.newPlaylistName)
                                                .then(createPlaylistResult => {
                                                    findPlaylistSongs(playlistToAdd.playlist, spotifySession)
                                                    .then(playlistInfo => {
                                                        spotifySession.addTracksToPlaylist(createPlaylistResult.body.id, playlistInfo.playlistTracks.map(el => el.track.trackURI))
                                                        .then(result => {
                                                            resolve()
                                                        })
                                                    })
                                                    .catch(e => {
                                                        console.log(e);
                                                        reject()
                                                    });
                                                })
                                                .catch(e => {
                                                    console.log(e);
                                                    reject()
                                                })
                                            });
                                            playlistToAddPromises.push(promiseToAdd);
                                        });
                                        Promise.all(playlistToAddPromises)
                                        .then(results => {
                                            createLog(dbResult.userID, true, forced, BackupSetting.NEW_PLAYLIST)
                                            .then(res => {
                                                resolve({ success: "Playlists Created!" })
                                            });
                                        })
                                    }
                                    else 
                                    {
                                        createLog(dbResult.userID, false, forced, BackupSetting.NEW_PLAYLIST, "Current Playlists Already Backed Up! Please Check Your Spotify Playlists.")
                                        .then(res => {
                                            reject({ error: "Current Playlists Already Backed Up! Please Check Your Spotify Playlists." })
                                        });
                                    }
                                })
                                .catch(e => {
                                    createLog(dbResult.userID, false, forced, BackupSetting.NEW_PLAYLIST, e.message)
                                    .then(res => {
                                        console.log(e);
                                        reject({ error: e.message })
                                    });
                                })
                            })
                            .catch(e => {
                                createLog(dbResult.userID, false, forced, BackupSetting.NEW_PLAYLIST, e.message)
                                .then(res => {
                                    console.log(e);
                                    reject({ error: e.message })
                                });
                            })
                        } catch (e) {
                            createLog(dbResult.userID, false, forced, BackupSetting.NEW_PLAYLIST, e.message)
                            .then(res => {
                                console.log(e);
                                reject({ error: e.message });
                            });
                        }
                        break;
                    }
                    case BackupSetting.APPEND_PLAYLIST: {
                        let promises: Promise<PlaylistInfo>[] = [];
                        for (const playlist in dbResult.playlists) {
                            promises.push(findPlaylistSongs(dbResult.playlists[playlist], spotifySession))
                        }
                        Promise.all(promises)
                            .then(responses => {
                                let allSongs: string[] = []
                                let backupSongs: string[] = []
                                responses.forEach(playlistRes => {
                                    allSongs.push(...playlistRes.playlistTracks.map(el => el.track.trackURI));
                                })

                                spotifySession.getPlaylistTracks(dbResult.backupPlaylistId!)
                                    .then(backupTracks => {

                                        let trackPromises = [];
                                        backupSongs.push(...backupTracks.body.items.map(el => el.track.uri));
                                        if (backupTracks.body.total > 100) {
                                            for (let i = 1; i < Math.ceil(backupTracks.body.total / 100); i++) {
                                                trackPromises.push(spotifySession.getPlaylistTracks(dbResult.backupPlaylistId!, { offset: 100 * i }))
                                            }
                                        }

                                        Promise.all(trackPromises)
                                            .then(trackResponses => {
                                                trackResponses.forEach(trackResponse => {
                                                    backupSongs.push(...trackResponse.body.items.map(el => el.track.uri))
                                                })
                                                const songsToAdd = allSongs.filter(el => !backupSongs.includes(el))
                                                if (songsToAdd.length > 0) {
                                                    new Promise((res) => {
                                                        let done = false;
                                                        while (!done) {
                                                            if (songsToAdd.length < 100) {
                                                                spotifySession.addTracksToPlaylist(dbResult.backupPlaylistId!, songsToAdd);
                                                                done = true;
                                                            }
                                                            else {
                                                                spotifySession.addTracksToPlaylist(dbResult.backupPlaylistId!, songsToAdd.splice(0, 100));
                                                            }
                                                        }
                                                        res();
                                                    })
                                                    .then(() => {
                                                        createLog(dbResult.userID, true, forced, BackupSetting.APPEND_PLAYLIST)
                                                        .then(res => {
                                                            resolve({ success: "Songs Added To Playlist!" })
                                                        });
                                                    });
                                                }
                                                else {
                                                    createLog(dbResult.userID, false, forced, BackupSetting.APPEND_PLAYLIST, "No New Songs To Add To Playlist!")
                                                    .then(res => {
                                                        reject({ error: "No New Songs To Add To Playlist!" })
                                                    });
                                                }
                                            })
                                            .catch(e => {
                                                createLog(dbResult.userID, false, forced, BackupSetting.APPEND_PLAYLIST, e.message)
                                                .then(res => {
                                                    console.log(e);
                                                    reject({ error: e.message })
                                                });
                                            })
                                    })
                                    .catch(e => {
                                        createLog(dbResult.userID, false, forced, BackupSetting.APPEND_PLAYLIST, e.message)
                                        .then(res => {
                                            console.log(e);
                                            reject({ error: e.message })
                                        });
                                    })
                            })
                            .catch(e => {
                                createLog(dbResult.userID, false, forced, BackupSetting.APPEND_PLAYLIST, e.message)
                                .then(res => {
                                    console.log(e);
                                    reject({ error: e.message })
                                });
                            });
                        break;
                    }
                    case BackupSetting.ADD_TO_LIBRARY: {
                        let promises: Promise<PlaylistInfo>[] = [];

                        for (const playlist in dbResult.playlists) {
                            promises.push(findPlaylistSongs(dbResult.playlists[playlist], spotifySession))
                        }
                        Promise.all(promises)
                            .then(responses => {
                                let allSongs: string[] = []
                                let savedSongs: string[] = []
                                responses.forEach(playlistRes => {
                                    allSongs.push(...playlistRes.playlistTracks.map(el => el.track.trackURI.split(":").pop()!));
                                })

                                spotifySession.getMySavedTracks({ limit: 50 })
                                    .then(savedTracks => {
                                        let trackPromises = [];
                                        savedSongs.push(...savedTracks.body.items.map(el => el.track.uri.split(":").pop()!));
                                        if (savedTracks.body.total > 50) {
                                            for (let i = 1; i < Math.ceil(savedTracks.body.total / 50); i++) {
                                                trackPromises.push(spotifySession.getMySavedTracks({ limit: 50, offset: 50 * i }))
                                            }
                                        }
                                        Promise.all(trackPromises)
                                            .then(trackResponses => {
                                                trackResponses.forEach(trackResponse => {
                                                    savedSongs.push(...trackResponse.body.items.map(el => el.track.uri))
                                                })
                                                const songsToAdd = allSongs.filter(el => !savedSongs.includes(el))
                                                if (songsToAdd.length > 0) {
                                                    new Promise((res) => {
                                                        let done = false;
                                                        while (!done) {
                                                            if (songsToAdd.length <= 50) {
                                                                spotifySession.addToMySavedTracks(songsToAdd);
                                                                done = true;
                                                            }
                                                            else {
                                                                spotifySession.addToMySavedTracks(songsToAdd.splice(0, 50));
                                                            }
                                                        }
                                                        res();
                                                    })
                                                        .then(() => {
                                                            createLog(dbResult.userID, true, forced, BackupSetting.ADD_TO_LIBRARY)
                                                            .then(res => {
                                                                resolve({ success: "Songs Added To Your Library!" })
                                                            });
                                                        })
                                                        .catch(e => {
                                                            createLog(dbResult.userID, false, forced, BackupSetting.ADD_TO_LIBRARY, e.message)
                                                            .then(res => {
                                                                console.log(e);
                                                                reject({ error: e })
                                                            });
                                                        })
                                                }
                                                else {
                                                    createLog(dbResult.userID, false, forced, BackupSetting.ADD_TO_LIBRARY, "No New Songs To Add To Your Library!")
                                                    .then(res => {
                                                        reject({ error: "No New Songs To Add To Your Library!" })
                                                    });
                                                }
                                            })
                                            .catch(e => {
                                                createLog(dbResult.userID, false, forced, BackupSetting.ADD_TO_LIBRARY, e.message)
                                                .then(res => {
                                                    console.log(e);
                                                    reject({ error: e.message })
                                                });
                                            });
                                    })
                                    .catch(e => {
                                        createLog(dbResult.userID, false, forced, BackupSetting.ADD_TO_LIBRARY, e.message)
                                        .then(res => {
                                            console.log(e);
                                            reject({ error: e.message })
                                        });
                                    })
                            })
                            .catch(e => {
                                createLog(dbResult.userID, false, forced, BackupSetting.ADD_TO_LIBRARY, e.message)
                                .then(res => {
                                    console.log(e);
                                    reject({ error: e.message })
                                });
                            });
                        break;
                    }
                }
            }
        });
    });
}

function ensureAuthenticated(req: any, res: any, next: any) {
    if (req.isAuthenticated()) {
        return next();
    }
    var redir = { redirect: "/" };
    return res.json(redir);
}

function ensureUnauthenticated(req: any, res: any, next: any) {
    if (!req.isAuthenticated()) {
        return next();
    }
    req.logout();
    req.session?.destroy((e: any)=> {if(e) console.log(e)});
    return next();
}

function ensureSpotifyTokenValid(req: any, res: any, next: any) {
    if (req.user) {
        User.findById(req.user._id, (err, result) => {
            if (result) {
                if (result.expires_in - Math.floor(Date.now() / 1000) < 0) {
                    refreshTokens(result._id)
                        .then(function (res) {
                            console.log(res);
                            return next();
                        })
                        .catch(function (e) {
                            console.log("Error Refreshing Tokens")
                            console.log(e);
                        });
                }
                else {
                    return next();
                }
            }
            else {
                console.log(err);
            }
        });
    }
}

const limtedBackup = bottleneck.wrap(backupPlaylists);
// 0 0 * * 3 - */5 * * * * *  
let backupJob = nodeSchedule.scheduleJob("backupPlaylists", "0 0 * * 4", () => {
    User.find((err, result) =>{
        if(result)
        {
            Promise.all(result.map(async (user) => {
                await refreshTokens(user._id)
                limtedBackup(null, user._id)
                .then(res => {
                    console.log(user.username + " Updated")
                })
                .catch(err => {
                    console.log(err);
                })
            }));
        }
    })
});

function refreshTokens(userId: string): Promise<{ accessToken: string, expires_in: number }> {
    return new Promise((resolve, reject) => {
        User.findById(userId, (err, result) => {
            if (result) {

                var data = {
                    refresh_token: result.refreshToken,
                    grant_type: 'refresh_token'
                }
        
                axios.post('https://accounts.spotify.com/api/token', queryString.stringify(data), {
                    headers: {
                        'Authorization': 'Basic ' + (Buffer.from(process.env.clientID + ':' + process.env.clientSecret).toString('base64'))
                    }
                })
                .then(function (response) {
                    result.accessToken = response.data.access_token;
                    result.expires_in = response.data.expires_in;
                    result.save()
                        .then(() => {
                            resolve()
                        }).catch(e => {
                            console.log(e);
                            throw e;
                        });
                })
                .catch(error => {
                    throw error;
                });
            }
            else {
                reject("User Not Found");
            }
        })
    });
}

function millisToMinutesAndSeconds(millis: number) {
    const minutes = Math.floor(millis / 60000);
    const seconds = ((millis % 60000) / 1000).toFixed(0);
    return (Number.parseInt(seconds) == 60 ? (minutes + 1) + ":00" : minutes + ":" + (Number.parseInt(seconds) < 10 ? "0" : "") + seconds);
}

async function createLog(userID: string, success: boolean, type: BackupType, setting: BackupSetting, message?: string)
{
    return BackupLog.create({userID: userID, backupTime: moment().valueOf(), success: success, 
        backupSetting: setting, backupType: type, backupMessage: message})
}

async function findPlaylistSongs(currPlaylist: PlaylistToBackup, spotifySession: Spotify) {
    const spotify = spotifySession;
    const res = await spotify.getPlaylistTracks(currPlaylist.playlistId);
    const playlistTracks: PlaylistTrackItem[] = [];
    let albumArts: string[] = [];
    let addedAt: string;
    res.body.items.forEach(track => {
        const trackArtists: ArtistItem[] = [];
        if (track.track) {
            track.track.artists.forEach(artist => {
                trackArtists.push({
                    artistName: artist.name,
                    artistURI: artist.uri
                })
            });

            playlistTracks.push({
                artist: trackArtists,
                track: {
                    trackName: track.track.name,
                    explicit: track.track.explicit,
                    trackURI: track.track.uri
                },
                album: {
                    albumName: track.track.album.name,
                    albumURI: track.track.album.uri
                },
                duration: millisToMinutesAndSeconds(track.track.duration_ms)
            })
            if (track.track.album.images[0].url !== "") {
                albumArts.push(track.track.album.images[0].url);
            }
            addedAt = track.added_at;
        }
    })

    const randomSelection = albumArts.slice(0, 4).map(() => {
        return albumArts.splice(Math.floor(Math.random() * albumArts.length), 1)[0];
    }, albumArts.slice());

    let weekNumber = moment(addedAt!).isoWeek();

    return textToImage.generate("Week " + weekNumber + "\n" + currPlaylist.playlistName.replace(" ", "\n"), {
        fontFamily: "Montserrat", fontSize: 40, customHeight: 500,
        maxWidth: 500, lineHeight: 10, textAlign: "left", textColor: '#ffffff'
    }, randomSelection)
        .then(albumImage => {
            return {
                playlistId: currPlaylist.playlistId,
                playlistName: currPlaylist.playlistName,
                playlistDesc: currPlaylist.playlistDesc,
                playlistURI: currPlaylist.playlistURI,
                playlistImage: albumImage,
                playlistDate: moment(addedAt).valueOf(),
                playlistTracks: playlistTracks
            };
        });
}

